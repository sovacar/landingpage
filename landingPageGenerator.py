import os



filename = "pod.html"

guacSSH = [ "https://guac.thewifilab.net/#/client/MTgAYwBwb3N0Z3Jlc3Fs", \
            "https://guac.thewifilab.net/#/client/MjAAYwBwb3N0Z3Jlc3Fs", \
            "https://guac.thewifilab.net/#/client/MTkAYwBwb3N0Z3Jlc3Fs", \
            "https://guac.thewifilab.net/#/client/MjEAYwBwb3N0Z3Jlc3Fs", \
            "https://guac.thewifilab.net/#/client/MjIAYwBwb3N0Z3Jlc3Fs", \
            "https://guac.thewifilab.net/#/client/MjMAYwBwb3N0Z3Jlc3Fs", \
            "https://guac.thewifilab.net/#/client/MjQAYwBwb3N0Z3Jlc3Fs", \
            "https://guac.thewifilab.net/#/client/MjUAYwBwb3N0Z3Jlc3Fs", \
            "https://guac.thewifilab.net/#/client/MjYAYwBwb3N0Z3Jlc3Fs", \
            "https://guac.thewifilab.net/#/client/MjcAYwBwb3N0Z3Jlc3Fs", \
            "https://guac.thewifilab.net/#/client/MjgAYwBwb3N0Z3Jlc3Fs", \
            "https://guac.thewifilab.net/#/client/MjkAYwBwb3N0Z3Jlc3Fs", \
            "https://guac.thewifilab.net/#/client/MzAAYwBwb3N0Z3Jlc3Fs", \
            "https://guac.thewifilab.net/#/client/MzEAYwBwb3N0Z3Jlc3Fs", \
            "https://guac.thewifilab.net/#/client/MzIAYwBwb3N0Z3Jlc3Fs" \
        ]

guacClient = [  "https://guac.thewifilab.net/#/client/MQBjAHBvc3RncmVzcWw%3D", \
                "https://guac.thewifilab.net/#/client/MgBjAHBvc3RncmVzcWw%3D", \
                "https://guac.thewifilab.net/#/client/NABjAHBvc3RncmVzcWw%3D", \
                "https://guac.thewifilab.net/#/client/NQBjAHBvc3RncmVzcWw%3D", \
                "https://guac.thewifilab.net/#/client/NgBjAHBvc3RncmVzcWw%3D", \
                "https://guac.thewifilab.net/#/client/NwBjAHBvc3RncmVzcWw%3D", \
                "https://guac.thewifilab.net/#/client/OABjAHBvc3RncmVzcWw%3D", \
                "https://guac.thewifilab.net/#/client/OQBjAHBvc3RncmVzcWw%3D", \
                "https://guac.thewifilab.net/#/client/MTAAYwBwb3N0Z3Jlc3Fs", \
                "https://guac.thewifilab.net/#/client/MTEAYwBwb3N0Z3Jlc3Fs", \
                "https://guac.thewifilab.net/#/client/MTIAYwBwb3N0Z3Jlc3Fs", \
                "https://guac.thewifilab.net/#/client/MTMAYwBwb3N0Z3Jlc3Fs", \
                "https://guac.thewifilab.net/#/client/MTQAYwBwb3N0Z3Jlc3Fs", \
                "https://guac.thewifilab.net/#/client/MTUAYwBwb3N0Z3Jlc3Fs", \
                "https://guac.thewifilab.net/#/client/MTYAYwBwb3N0Z3Jlc3Fs" \
        ]

guacClienbackup = ["https://backup.guac.thewifilab.net/#/client/MgBjAHBvc3RncmVzcWw%3D", \
                    "https://backup.guac.thewifilab.net/#/client/MwBjAHBvc3RncmVzcWw%3D", \
                    "https://backup.guac.thewifilab.net/#/client/NABjAHBvc3RncmVzcWw%3D", \
                    "https://backup.guac.thewifilab.net/#/client/NQBjAHBvc3RncmVzcWw%3D", \
                    "https://backup.guac.thewifilab.net/#/client/NgBjAHBvc3RncmVzcWw%3D", \
                    "https://backup.guac.thewifilab.net/#/client/NwBjAHBvc3RncmVzcWw%3D", \
                    "https://backup.guac.thewifilab.net/#/client/OABjAHBvc3RncmVzcWw%3D", \
                    "https://backup.guac.thewifilab.net/#/client/OQBjAHBvc3RncmVzcWw%3D", \
                    "https://backup.guac.thewifilab.net/#/client/MTAAYwBwb3N0Z3Jlc3Fs", \
                    "https://backup.guac.thewifilab.net/#/client/MTEAYwBwb3N0Z3Jlc3Fs", \
                    "https://backup.guac.thewifilab.net/#/client/MTIAYwBwb3N0Z3Jlc3Fs", \
                    "https://backup.guac.thewifilab.net/#/client/MTMAYwBwb3N0Z3Jlc3Fs", \
                    "https://backup.guac.thewifilab.net/#/client/MTQAYwBwb3N0Z3Jlc3Fs", \
                    "https://backup.guac.thewifilab.net/#/client/MTUAYwBwb3N0Z3Jlc3Fs", \
                    "https://backup.guac.thewifilab.net/#/client/MTYAYwBwb3N0Z3Jlc3Fs", \
                    "https://backup.guac.thewifilab.net/#/client/MTcAYwBwb3N0Z3Jlc3Fs" \


        ]

fout = [0]*15

for i in range(15):
    fout[i] = open("pod" + str(i+1) + ".html", "w+")



for i in range(15):
    
    #output file to write the result to
    fin = open("pod.html", "rt")
    #fout[i] = open("pod" + str(i+1) + ".html", "w+")
    
    #for each line in the input file
    for line in fin:
        #read replace the string and write to output file
        fout[i].write(line.\
        
        replace('Number X', 'Number ' + str(i+1)). \
        replace('guideLink', 'https://guide.thewifilab.net/pod' + str(i+1) + '/home.html').\
        
        replace('-X', '-' + str(i+1)). \
        replace('wlcX', 'wlc' + str(i+1)). \
        replace('iseX', 'ise' + str(i+1)). \
        replace('podX', 'pod' + str(i+1)). \
        replace('guacSSH', guacSSH[i]). \
        replace('guacClient', guacClient[i]). \
        replace('guacClienbackup', guacClienbackup[i]) \
        )
    fout[i].close()
    fin.close()
        #close input and output files




